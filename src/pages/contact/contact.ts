import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {EmailComposer} from "@ionic-native/email-composer";

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  info = {
    'address':'121, Clear Water Bay Road',
    'region': 'Clear Water Bay, Kowloon',
    'city': 'HONG KONG',
    'telnum': '+852 1234 5678',
    'faxnum': '+852 8765 4321',
    'email':'confusion@food.net'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private emailComposer: EmailComposer) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

  sendEmail(){
    let email = {
      to: 'confusion@food.net',
      subject: '[ConFusion]: Query',
      body: 'Dear Sir/Madam:',
      isHtml: true
    };

    this.emailComposer.open(email);
  }

}
