import { Component, Inject, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Leader} from "../../shared/leader";
import {LeaderProvider} from "../../providers/leader/leader";

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage implements OnInit{

  leaders: Leader[];
  errMess: string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private leaderservice: LeaderProvider,
              @Inject('BaseURL') public baseURL) {
  }

  ngOnInit(){
    this.leaderservice.getLeaders()
      .subscribe(
        leaders => this.leaders = leaders,
        errMess => this.errMess = <any> errMess
      );
  }

  historyInfo = "Started in 2010, Ristorante con Fusion quickly established itself as a culinary icon par excellence in Hong Kong. With its unique brand of world fusion cuisine that can be found nowhere else, it enjoys patronage from the A-list clientele in Hong Kong.  Featuring four of the best three-star Michelin chefs in the world, you never know what will arrive on your plate the next time you visit us. The restaurant traces its humble beginnings to The Frying Pan, a successful chain started by our CEO, Mr. Peter Pan, that featured for the first time the world's best cuisines in a pan."

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

}
