import { Component, } from '@angular/core';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import {Comment} from "../../shared/comment";

/**
 * Generated class for the CommentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {

  comment: Comment;
  commentForm: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public viewCtrl: ViewController,
              private formBuilder: FormBuilder) {
    this.createForms();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentsPage');
  }

  createForms(){
    this.commentForm = this.formBuilder.group({
      author: ['', Validators.required],
      comment: ['', Validators.required],
      rating: 5,
      date:''
    });
  }


  onSubmit(){
    this.comment = this.commentForm.value;
    var date = new Date().toISOString();
    this.comment.date = date;
    this.viewCtrl.dismiss(this.comment);
  }



  dismiss() {
    this.viewCtrl.dismiss();
  }
}
